'use strict';
import React, {Component}  from 'react';
//import Rap from './ncrRapClientLib';
import {Login} from './login';
import { Container, Header, Content, Icon, Card, CardItem, Body, Text } from 'native-base';
import {observer, inject} from 'mobx-react/native';

@inject("rapProperties")
@observer
export class StatusCard extends Component {

  render() {
    return (
            <Card >
              <CardItem button onPress={this.onPress.bind(this)}>
                <Body>
                  <Icon name='pulse' />
                  <Text>
                    User: {this.props.rapProperties.currentOperator}
                  </Text>
                  <Text note>{this.props.rapProperties.connectStatus}</Text>
                </Body>
              </CardItem>
            </Card>
    );
  }

  onPress() {
   // this.props.rapProperties.currentOperator = 'test';
    this.props.navigation.navigate('Login');
  }
}
